const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "ban",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.hasPermission(0x00000004)) return message.reply("You are missing permission `BAN_MEMBERS`").then(m => m.delete({timeout: 10000}));
        if(!message.guild.me.hasPermission(0x00000004)) return message.reply("I don't have permission `BAN_MEMBERS`").then(m => m.delete({timeout: 10000}));
        if(!args[0]) return message.reply("Please specify the member you want to ban\n`o!ban <@/userID> [reason]`").then(m => m.delete({timeout: 10000}));

        const toBan = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
        if(!toBan) return message.reply(`Can't find member "${args[0]}", make sure you typed the name correctly or the ID correctly.`).then(m => m.delete({timeout: 10000}));

        if(args[1]){
            const embed = new MessageEmbed()
                .setDescription(`🔨 ${toBan.user.tag} has been banned by ${message.author.tag}\nReason: ${args.slice(1).join(" ")}`)
                .setColor("#ff1919")
                .setTimestamp();

            const embedDM = new MessageEmbed()
                .setTitle(`You were banned in ${message.guild.name}`)
                .setColor("#ff1919")
                .setThumbnail(message.guild.iconURL())
                .addFields(
                    {name:"Banned by", value:message.author.tag},
                    {name:"Reason", value: args.slice(1).join(" ")}
                )

            toBan.send(embedDM);
            toBan.ban({reason: args.slice(1).join(" ")})
            return message.channel.send(embed)

        } else {
            const embed = new MessageEmbed()
                .setDescription(`🔨 ${toBan.user.tag} has been banned by ${message.author.tag}`)
                .setColor("#ff1919")
                .setTimestamp();

            const embedDM = new MessageEmbed()
                .setTitle(`You were banned in ${message.guild.name}`)
                .setColor("#ff1919")
                .setThumbnail(message.guild.iconURL())
                .addFields(
                    {name:"Banned by", value:message.author.tag},
                    {name:"Reason", value: "Ban hammer has spoken!"}
                )

            toBan.send(embedDM);
            toBan.ban({reason: 'No reason provided'});
            return message.channel.send(embed);
        }

    }
}