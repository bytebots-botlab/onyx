const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "purge",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.hasPermission(0x00000002)) return message.reply("You are missing permission `KICK_MEMBERS`").then(m => m.delete({timeout: 10000}));
        if(!message.guild.me.hasPermission(0x00000002)) return message.reply("I don't have permission `MANAGE_MESSAGES`").then(m => m.delete({timeout: 10000}));
        if(!args[0]) return message.reply("Please specify amount of messages to delete\n`o!purge <amount>`").then(m => m.delete({timeout: 10000}));
        if(isNaN(args[0])) return message.reply(`"${args[0]}" is not a number\n\`o!purge <amount>\``)
        if(args[0] > 100) return message.reply(`Choose a number between 1-100\n\`o!purge <amount>\``)
    
        message.channel.bulkDelete(args[0])
        .then(msgs => {
            const embed = new MessageEmbed()
                .setDescription(`🗑 ${message.author} purged \`${msgs.size}\` message(s)`)
                .setColor("#808080")
                .setTimestamp()

            message.channel.send(embed);
        });
    }
}