const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "warn",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.hasPermission(0x00000002)) return message.reply("You are missing permission `KICK_MEMBERS`").then(m => m.delete({timeout: 10000}));
        if(!args[0]) return message.reply("Please specify the member you want to warn\n`o!warn <@/userID> <reason>`").then(m => m.delete({timeout: 10000}));
        if(!args[1]) return message.reply("Please specify the reason for warning\n`o!warn <@/userID> <reason>`").then(m => m.delete({timeout: 10000}));

        const toWarn = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
        if(!toWarn) return message.reply(`Can't find member "${args[0]}", make sure you typed the name correctly or the ID correctly.`).then(m => m.delete({timeout: 10000}));

            const embed = new MessageEmbed()
                .setDescription(`:warning: ${toWarn.user.tag} has been warned by ${message.author.tag}\nReason: ${args.slice(1).join(" ")}`)
                .setColor("#ff1919")
                .setTimestamp();

            const embedDM = new MessageEmbed()
                .setTitle(`You were warned in ${message.guild.name}`)
                .setColor("#ff1919")
                .setThumbnail(message.guild.iconURL())
                .addFields(
                    {name:"Warned by", value:message.author.tag},
                    {name:"Reason", value: args.slice(1).join(" ")}
                )

            toWarn.send(embedDM);
            return message.channel.send(embed)
    }
}