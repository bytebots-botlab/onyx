const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "kick",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.hasPermission(0x00000002)) return message.reply("You are missing permission `KICK_MEMBERS`").then(m => m.delete({timeout: 10000}));
        if(!message.guild.me.hasPermission(0x00000002)) return message.reply("I don't have permission `KICK_MEMBERS`").then(m => m.delete({timeout: 10000}));
        if(!args[0]) return message.reply("Please specify the member you want to kick\n`o!kick <@/userID> [reason]`").then(m => m.delete({timeout: 10000}));

        const toKick = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
        if(!toKick) return message.reply(`Can't find member "${args[0]}", make sure you typed the name correctly or the ID correctly.`).then(m => m.delete({timeout: 10000}));

        if(args[1]){
            const embed = new MessageEmbed()
                .setDescription(`👞 ${toKick.user.tag} has been kicked by ${message.author.tag}\nReason: ${args.slice(1).join(" ")}`)
                .setColor("#ff1919")
                .setTimestamp();

            const embedDM = new MessageEmbed()
                .setTitle(`You were kicked in ${message.guild.name}`)
                .setColor("#ff1919")
                .setThumbnail(message.guild.iconURL())
                .addFields(
                    {name:"Kicked by", value:message.author.tag},
                    {name:"Reason", value: args.slice(1).join(" ")}
                )

            toKick.send(embedDM);
            toKick.kick({reason: args.slice(1).join(" ")})
            return message.channel.send(embed)

        } else {
            const embed = new MessageEmbed()
                .setDescription(`👞 ${toKick.user.tag} has been kicked by ${message.author.tag}`)
                .setColor("#ff1919")
                .setTimestamp();

            const embedDM = new MessageEmbed()
                .setTitle(`You were kicked in ${message.guild.name}`)
                .setColor("#ff1919")
                .setThumbnail(message.guild.iconURL())
                .addFields(
                    {name:"Kicked by", value:message.author.tag}
                )

            toKick.send(embedDM);
            toKick.kick({reason: 'No reason provided'});
            return message.channel.send(embed);
        }

    }
}