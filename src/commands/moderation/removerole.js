const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "removerole",
    run: async (client, message, args) => {
        message.delete()

        if(!message.member.hasPermission(0x10000000)) return message.reply("You are missing permission `MANAGE_ROLES`").then(m => m.delete({timeout: 10000}));
        if(!message.guild.me.hasPermission(0x10000000)) return message.reply("I don't have permission `MANAGE_ROLES`").then(m => m.delete({timeout: 10000}));
        if(!args[0]) return message.reply("Please specify the member you want to take the role from\n`o!removerole <@/userID> <role>`").then(m => m.delete({timeout: 10000}));
        if(!args[1]) return message.reply("Please specify the role you want to take from the member\n`o!removerole <@/userID> <role>`").then(m => m.delete({timeout: 10000}));

        const target = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
        if(!target) return message.reply(`Can't find member "${args[0]}", make sure you typed the name correctly or the ID correctly.`).then(m => m.delete({timeout: 10000}));

        const targetRole = message.mentions.roles.first() || message.guild.roles.cache.get(args[1]);
        if(!targetRole) return message.reply(`Can't find role "${args[1]}", make sure you typed the name correctly or the ID correctly.`).then(m => m.delete({timeout: 10000}));

        if(!target.roles.cache.find(r => r.id === targetRole.id)) return message.reply(`${target} does not have ${targetRole} role.`).then(m => m.delete({timeout: 10000}));

        const embed = new MessageEmbed()
            .setDescription(`<a:approved:765040824941281311> ${message.author} took ${targetRole} role from ${target}`)
            .setColor("GREEN")
            .setTimestamp()

        target.roles.remove(targetRole);
        message.channel.send(embed);
    }
}