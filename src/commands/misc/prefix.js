const Discord = require("discord.js");
const fs = require("fs");

module.exports = {
    name: "prefix",
    run: async (client, message, args) => {
        message.delete()

        let prefixes = JSON.parse(fs.readFileSync("./src/data/prefixes.json", "utf8"));
        if(!message.member.hasPermission(0x00000020)) return message.reply("You are missing permission `MANAGE_SERVER`");
        if(!args[0]){
            if(prefixes[message.guild.id]){
                const embed = new Discord.MessageEmbed()
                .setColor("#4f4c5f")
                    .setTitle("ONYX Prefix")
                    .setThumbnail(client.user.displayAvatarURL())
                    .setDescription(`Prefix is currently \`${prefixes[message.guild.id].prefixes}\``)
                    .setTimestamp();

                return message.channel.send(embed)
            } else {
                const embed = new Discord.MessageEmbed()
                .setColor("#4f4c5f")
                .setTitle("ONYX Prefix")
                .setThumbnail(client.user.displayAvatarURL())
                .setDescription(`Prefix is currently \`o!\``)
                .setTimestamp();

                return message.channel.send(embed)
            }
        }

        prefixes[message.guild.id] = {
            prefixes: args[0]
        };

        fs.writeFile("./src/data/prefixes.json", JSON.stringify(prefixes), (err) => {
            if (err) console.log(err)
        });

        let prefixEmbed = new Discord.MessageEmbed()
            .setColor("#4f4c5f")
            .setThumbnail(client.user.displayAvatarURL())
            .setTitle("Prefix for ONYX has been changed")
            .setDescription(`New prefix ${args[0]}`)
            .setFooter(`Prefix changed by ${message.member.user.tag}`, message.author.displayAvatarURL())
            .setTimestamp();

        message.channel.send(prefixEmbed);
    }
}