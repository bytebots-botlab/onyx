const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "invite",
    run: async (client, message, args) => {
        message.delete()

        if(!message.guild.me.hasPermission(0x00000001)) return message.reply("I don't have permission `CREATE_INSTANT_INVITE`").then(m => m.delete({timeout: 10000}));

        message.channel.createInvite({maxAge: 86400, reason: `${message.member.user.tag} created invite`})
        .then(invite => {
            const embed = new MessageEmbed()
                .setTitle(`New invitation link created`)
                .addFields(
                    {name: "Invite", value: `${invite}`, inline:true},
                    {name: "Created by", value: message.member.user.tag, inline:true},
                    {name: "Expires", value: `24 hours`}
                    )
                .setThumbnail(message.guild.iconURL())
                .setTimestamp()
                .setColor(message.member.displayColor)
                .setFooter(`Invite created by ${message.author.tag}`, message.author.displayAvatarURL());

            message.channel.send(embed);
        })
            
    }
}