const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "help",
    run: async (client, message, args) => {
        message.delete()

        if(!args[0]){
            const defaulthelp = new MessageEmbed()
                .setTitle(`ONYX Help`)
                .setColor("#4f4c5f")
                .setThumbnail(client.user.displayAvatarURL())
                .setTimestamp()
                .setFooter(`${message.member.user.tag} asked for help`)
                .addFields(
                    {name: "😂 Fun commands", value: "`o!help fun`\nAll fun and miscellaneous commands", inline:true},
                    {name: "✨ Miscellaneous commands", value: "`o!help misc`\nAll prefix, bug reports and other similar commands", inline:true},
                    { name: '​', value: '​' },
                    {name: "🔨 Moderation commands", value: "`o!help moderation`\nAll moderation commands",inline:true},
                    {name: "🔞 NSFW commands", value: "`o!help nsfw`\nAll NSFW commands", inline:true},
                    { name: '​', value: '​' },
                    {name: "Invite ONYX", value: "[Click to invite](https://discord.com/api/oauth2/authorize?client_id=769386444909051935&permissions=268823623&scope=bot)", inline:true},
                    {name: "Join support server", value: "[Click to join](https://bytebots.xyz/invite)", inline:true}
                )

            message.channel.send(defaulthelp);
        }
        if(args[0] === 'fun'){
            const funhelp = new MessageEmbed()
                .setTitle(`ONYX 😂 Fun commands`)
                .setTimestamp()
                .setColor("#4f4c5f")
                .setDescription("`<required>`\n`[optional]`")
                .setThumbnail(client.user.displayAvatarURL())
                .setFooter(`${message.member.user.tag} asked for help with fun`)
                .addFields(
                    {name: "Cats", value: "`o!cats`\nRandom cat pictures from r/cats"},
                    {name: "Dogs", value: "`o!dogs`\nRandom dog pictures from r/lookatmydog"},
                    {name: "Cute animals", value: "`o!awww`\nRandom cute animal pictures from r/awww"},
                    {name: "Dad jokes", value: "`o!dadjoke`\nGives you random dad joke"},
                    {name: "Chuck Norris joke", value: "`o!chucknorris`\nGives you random Chuck Norris joke"},
                    {name: "Hug", value: "`o!hug [@/user]`\nHug someone"},
                    {name: "Kiss", value: "`o!kiss [@/user]`\nKiss someone"},
                    {name: "Slap", value: "`o!slap [@/user]`\nSlap someone"}
                );

            message.channel.send(funhelp);
        }
        if(args[0] === 'misc'){
            const funhelp = new MessageEmbed()
                .setTitle(`ONYX ✨ Misc commands`)
                .setTimestamp()
                .setColor("#4f4c5f")
                .setDescription("`<required>`\n`[optional]`")
                .setThumbnail(client.user.displayAvatarURL())
                .setFooter(`${message.member.user.tag} asked for help with fun`)
                .addFields(
                    {name: "Bug report", value: "`o!bug <report/issue/bug>`\nSends bug report straight to our support server."},
                    {name: "Prefix", value: "`o!prefix [new prefix]`\nCheck or set new prefix for the server."}
                );

            message.channel.send(funhelp);
        }
        if(args[0] === 'moderation'){
            const funhelp = new MessageEmbed()
                .setTitle(`ONYX 🔨 Moderation commands`)
                .setTimestamp()
                .setColor("#4f4c5f")
                .setDescription("`<required>`\n`[optional]`")
                .setThumbnail(client.user.displayAvatarURL())
                .setFooter(`${message.member.user.tag} asked for help with moderation`)
                .addFields(
                    {name: "Add role to user", value:"`o!addrole <@/userID> <role>`\nGives user a specified role"},
                    {name: "Remove role from user", value:"`o!removerole <@/userID> <role>`\nRemoves a specified role from user"},
                    {name: "Ban", value: "`o!ban <@/userID> [reason]`\nBan user from the server, with or without reason"},
                    {name: "Kick", value: "`o!kick <@/userID> [reason]`\nKick user from the server, with or without reason"},
                    {name: "Purge", value: "`o!purge <amount>`\nMass delete messages from the channel"},
                    {name: "Warning", value: "`o!warn <@/userID> <reason>`\nGive warning to user in the server with reason"},
                );

            message.channel.send(funhelp);
        }
        if(args[0] === 'nsfw'){
            if(!message.channel.nsfw) return message.reply("There is kids here..\nMark channel as `NSFW` to use this command.").then(m => m.delete({timeout: 10000}))
            const nsfwhelp = new MessageEmbed()
                .setTitle(`ONYX 🔞 NSFW commands`)
                .setTimestamp()
                .setThumbnail(client.user.displayAvatarURL())
                .setColor("#4f4c5f")
                .setDescription("`<required>`\n`[optional]`")
                .setFooter(`${message.member.user.tag} asked for help with "fun"`)
                .addFields(
                    {name: "Dick", value: "`o!dick`\nRandom dick pictures from r/MassiveCock"},
                    {name: "Female butt", value: "`o!femalebutt`\nRandom female butt pictures from r/ass"},
                    {name: "Male butt", value: "`o!malebutt`\nRandom male butt pictures from r/gayass"},
                    {name: "Pussy", value: "`o!pussy`\nRandom pussy pictures from r/pussy"},
                    {name: "Tits", value: "`o!tits`\nRandom boobie pictures from r/tits"},
                    {name: "Hentai", value: "`o!hentai`\nRandom hentai pictures from r/hentai"},
                    {name: "Futanari", value: "`o!futa`\nRandom futanari pictures form r/futanari"},
                    {name: "Yaoi", value: "`o!yaoi`\nRandom yaoi pictures from r/yaoi"}
                );

            message.channel.send(nsfwhelp);
        }
    }
}