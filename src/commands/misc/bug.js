const {MessageEmbed} = require('discord.js');
module.exports = {
    name: "bug",
    run: async (client, message, args) => {
        message.delete()

        if(!args[0]) return message.reply("You must specify the reason for bug report\n`o!bug <reason/issue>`").then(m => m.delete({timeout: 10000}))

        const embed = new MessageEmbed()
            .setTitle("<a:approved:765040824941281311> Bug report sent successfully")
            .setDescription("Thank you for your contribution to help make ONYX more seamless and bug free.")
            .setColor("#28A745")
            .setThumbnail(client.user.displayAvatarURL())
            .addFields(
                {name: "Your report", value: args.slice(0).join(" ")},
                {name: "Our support server", value: "[Click to join](https://bytebots.xyz/invite)", inline:true},
                {name: "Our website", value: "[Click me to check](https://bytebots.xyz)", inline:true}
            )
            .setTimestamp()
            .setFooter(`Bug reported by ${message.author.tag}`, message.member.user.displayAvatarURL());

        const embedch = new MessageEmbed()
            .setTitle("ONYX bug report")
            .setColor("#FF0000")
            .setThumbnail(message.member.user.displayAvatarURL())
            .addFields(
                {name: "Reported by", value: `${message.author.tag} (${message.member.user.id})`, inline:true},
                {name: "From server", value: `${message.guild.name} (${message.guild.id})`, inline:true},
                {name: "Issue", value: args.slice(0).join(" ")}
            )
            .setTimestamp();

        const bytebotsguild = client.guilds.cache.get('764248014033780817')
        bytebotsguild.channels.cache.get('764583287145627679').send('<@&772121477159190538> <@&765716823978147871>')
        bytebotsguild.channels.cache.get('764583287145627679').send(embedch)
        message.channel.send(embed)
            
    }
}