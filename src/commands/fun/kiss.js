const {Discord, MessageEmbed} = require('discord.js');
module.exports = {
    name: "kiss",
    run: async (client, message, args) => {
        message.delete()

        const kisss = ["https://bytebots.xyz/src/img/onyx/kiss1.gif", "https://bytebots.xyz/src/img/onyx/kiss2.gif", "https://bytebots.xyz/src/img/onyx/kiss3.gif", "https://bytebots.xyz/src/img/onyx/kiss4.gif", "https://bytebots.xyz/src/img/onyx/kiss5.gif"]
        const random = Math.floor(Math.random() * kisss.length);
        if(!args[0]){
            
            const embed = new MessageEmbed()
                .setDescription(`ONYX smooched ${message.author}`)
                .setImage(kisss[random])
                .setColor(message.member.displayColor)
                .setTimestamp()

            message.channel.send(embed)
        } else{
            let target = message.mentions.members.first() || message.guild.members.cache.get(args[0]);

            if(!target){
                const embed = new MessageEmbed()
                .setDescription(`ONYX smooched ${message.author}`)
                .setColor(message.member.displayColor)
                .setImage(kisss[random])
                .setTimestamp()

                return message.channel.send(embed)
            }

            const embed = new MessageEmbed()
                .setDescription(`${message.author} smooched ${target.user}`)
                .setImage(kisss[random])
                .setColor(message.member.displayColor)
                .setTimestamp()
                .setFooter(`o!kiss ${message.author.tag} to kiss them back!`)

            return message.channel.send(embed)

        }
    }
}
