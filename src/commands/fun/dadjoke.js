const giveMeAJoke = require('give-me-a-joke');
module.exports = {
    name: "dadjoke",
    run: async (client, message, args) => {
        message.delete()

        giveMeAJoke.getRandomDadJoke (function(joke) {
            message.reply(joke + " :drum:")
        });
    }
}
