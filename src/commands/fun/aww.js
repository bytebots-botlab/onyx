const {Discord, MessageEmbed} = require('discord.js');
const redditFetch = require('reddit-fetch');
module.exports = {
    name: "aww",
    run: async (client, message, args) => {
        message.delete()
        const redditFetch = require('reddit-fetch');

        redditFetch({
        
            subreddit: 'aww',
            sort: 'hot',
            allowNSFW: false,
            allowModPost: true,
            allowCrossPost: true,
        
        }).then(post => {
        const embed = new MessageEmbed()
        .setTitle(`🐱 r/aww`)
        .setColor(message.member.displayColor)
        .setDescription(post.title)
        .setURL(post.url)
        .setImage(post.url)
        .setFooter(message.member.displayName, message.author.displayAvatarURL())
        .setTimestamp();
        message.channel.send(embed);

        })
    }
}
