const {Discord, MessageEmbed} = require('discord.js');
module.exports = {
    name: "slap",
    run: async (client, message, args) => {
        message.delete()

        const slaps = ["https://bytebots.xyz/src/img/onyx/slap1.gif", "https://bytebots.xyz/src/img/onyx/slap2.gif", "https://bytebots.xyz/src/img/onyx/slap3.gif", "https://bytebots.xyz/src/img/onyx/slap4.gif", "https://bytebots.xyz/src/img/onyx/slap5.gif"]
        const random = Math.floor(Math.random() * slaps.length);
        if(!args[0]){
            
            const embed = new MessageEmbed()
                .setDescription(`ONYX just slapped ${message.author}`)
                .setImage(slaps[random])
                .setTimestamp()
                .setColor(message.member.displayColor)

            message.channel.send(embed)
        } else{
            let target = message.mentions.members.first() || message.guild.members.cache.get(args[0]);

            if(!target){
                const embed = new MessageEmbed()
                .setDescription(`ONYX just slapped ${message.author}`)
                .setImage(slaps[random])
                .setColor(message.member.displayColor)
                .setTimestamp()

                return message.channel.send(embed)
            }

            const embed = new MessageEmbed()
                .setDescription(`${message.author} just slapped ${target.user}`)
                .setImage(slaps[random])
                .setTimestamp()
                .setColor(message.member.displayColor)
                .setFooter(`o!slap ${message.author.tag} to slap them back!`)

            return message.channel.send(embed)

        }
    }
}
