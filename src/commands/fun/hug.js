const {Discord, MessageEmbed} = require('discord.js');
module.exports = {
    name: "hug",
    run: async (client, message, args) => {
        message.delete()

        const hugs = ["https://bytebots.xyz/src/img/onyx/hug1.gif", "https://bytebots.xyz/src/img/onyx/hug2.gif", "https://bytebots.xyz/src/img/onyx/hug3.gif", "https://bytebots.xyz/src/img/onyx/hug4.gif", "https://bytebots.xyz/src/img/onyx/hug5.gif"]
        const random = Math.floor(Math.random() * hugs.length);
        if(!args[0]){
            
            const embed = new MessageEmbed()
                .setDescription(`ONYX gave big sweet hug to ${message.author}`)
                .setImage(hugs[random])
                .setTimestamp()
                .setColor(message.member.displayColor)

            message.channel.send(embed)
        } else{
            let target = message.mentions.members.first() || message.guild.members.cache.get(args[0]);

            if(!target){
                const embed = new MessageEmbed()
                .setDescription(`ONYX gave big sweet hug to ${message.author}`)
                .setImage(hugs[random])
                .setColor(message.member.displayColor)
                .setTimestamp()

                return message.channel.send(embed)
            }

            const embed = new MessageEmbed()
                .setDescription(`${message.author} gave big sweet hug to ${target.user}`)
                .setImage(hugs[random])
                .setTimestamp()
                .setColor(message.member.displayColor)
                .setFooter(`o!hug ${message.author.tag} to hug them back!`)

            return message.channel.send(embed)

        }
    }
}
