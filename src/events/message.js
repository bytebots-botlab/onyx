const { MessageEmbed, MessageCollector } = require('discord.js');
const fs = require('fs');

module.exports = async (client, message) => {
    
    let prefixes = JSON.parse(fs.readFileSync("./src/data/prefixes.json", "utf8"));

    if(message.channel.type === "dm") return;

    if (!prefixes[message.guild.id]){
        prefixes[message.guild.id] = {
            prefixes: "o!"
        };
    }
    let prefix = prefixes[message.guild.id].prefixes;
    // Make sure the message contains the command prefix from the config.json.
    if (!message.content.startsWith(prefix)) return;
    // Make sure the message author isn't a bot.
    if (message.author.bot) return;
    // Make sure the channel the command is called in is a text channel.
    if (message.channel.type !== 'text') return;

    /* Split the message content and store the command called, and the args.
    * The message will be split using space as arg separator.
    */
    const cmd = message.content.split(/\s+/g)[0].slice(prefix.length);
    const args = message.content.split(/\s+/g).slice(1);

    try {
        // Check if the command called exists in either the commands Collection
        // or the aliases Collection.
        let command;
        if (client.commands.has(cmd)) {
            command = client.commands.get(cmd);
        } else if (client.aliases.has(cmd)) {
            command = client.commands.get(client.aliases.get(cmd));
        }

        // Make sure command is defined.
        if (!command) return;

        // If the command exists then run the execute function inside the command file.
        command.run(client, message, args);
        console.log(`Ran command: ${command.name}`); // Print the command that was executed.
    } catch (err) {
        console.error(err);
    }
};