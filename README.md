<div align="center">
<img width="800" src="https://i.imgur.com/pB4XWdv.png">

**ONYX is ByteBots' Botlab's open source multipurpose bot, written mainly using discordJS as the base.**

## Features

ONYX has many different features for users and admins of the server.
To browse commands just simply use **o!help** command. 

If you want to partake development of ONYX just go ahead and open up merge request once your feature is ready to be implemented to ONYX.

<img width="800" src="https://i.imgur.com/uQQTlup.png">

## Support

Support for ONYX is offered on our [official discord server](https://bytebots.xyz/invite), you are more than welcome to join our discord server even if you are just looking for help with your own programs or bots, we are more than happy to assist you if you bring cookies! You can also [open issues](https://gitlab.com/bytebots-botlab/onyx/-/issues) here on Gitlab, the issue does not need to necceseraly issue but it can also be feature request.

## Copyrights

You are more than welcome to use parts of ONYX's code on your own project, just remember to mention **"ByteBots Botlab"** for example in your Git's README.md file. That's all we ask, some credits for our hard work that we are doing with no pay.
</div>